package testGreeter;

import org.junit.Test;
import static org.hamcrest.CoreMatchers.containsString;
import hello.Greeter;

import static org.junit.Assert.*;

public class GreeterTest{
	private Greeter greeter = new Greeter();
	@Test
	public void greeterSaysHello(){
		assertThat(greeter.sayHello(), containsString("Hello"));
	}
}

